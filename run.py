#!/usr/bin/python3

import sys
import os
import textwrap
import time
import itertools

from execo import *
from execo_g5k import *


def reserve_resources(cluster='nova', num_backends=3, existing_jobsite=None):
    if not existing_jobsite:
        logger.info("Submitting OAR job...")
        jobid, site = oarsub([
            (OarSubmission(resources="nodes=%d" % int(num_backends+2),
                          walltime="1:00:00",
                          job_type="deploy",
                          sql_properties="cluster='%s'" % cluster
                    ), get_cluster_site(cluster))
            ], abort_on_error=True)[0]
    else:
        jobid, site = existing_jobsite
        logger.info("Use existing OAR job")

    logger.info("Getting OAR node list from %s@%s..." % (jobid, site))
    nodes = sorted(get_oar_job_nodes(jobid, site, timeout=180),
                   key=lambda h: str(h))
    if not nodes:
        oardel([(jobid,site)])
        raise Exception("Job start timeouted")

    logger.info("Node list is %s" % nodes)
    return nodes[0], nodes[1], nodes[2:]


def install_resources(client, lb, backends):
    logger.info("Deploying nodes...")
    _, ko_nodes = deploy(Deployment(
                    env_name="jessie-x64-base",
                    hosts=[client]+[lb]+backends
                    ))
    if ko_nodes:
        raise Exception("Deployment failed on nodes: %s" % ko_nodes)

    logger.info("Installing nodes...")
    client_cfg = textwrap.dedent("""\
        from locust import HttpLocust, TaskSet, task
        
        class UserBehavior(TaskSet):
            @task
            def get(self):
                self.client.get("/")
        
        class WebsiteUser(HttpLocust):
            task_set = UserBehavior
            min_wait = 3000
            max_wait = 9000
    """)

    client_cmd = Remote(" && ".join((
            "apt update",
            "apt install -y python-setuptools python-dev libzmq-dev build-essential",
            "easy_install locustio",
            "echo '%s' > /opt/locustfile.py" % client_cfg)),
        [client], connection_params={'user': 'root'}).start()
    lb_cmd = Remote(" && ".join((
            "systemctl start ganglia-monitor",
            "apt update",
            "apt install -y nginx-full")),
        [lb], connection_params={'user': 'root'}).start()
    backends_cmd = Remote(" && ".join((
            "systemctl start ganglia-monitor",
            "apt update",
            "apt install -y apt-transport-https",
            "echo 'deb [arch=amd64] https://download.docker.com/linux/debian jessie stable'" \
            " > /etc/apt/sources.list.d/docker.list",
            "wget -O- https://download.docker.com/linux/debian/gpg | apt-key add -",
            "apt update",
            "apt install -y docker-ce",
            "mkdir -p /tmp/ipynb",
            "docker pull jupyter/nbviewer")),
        backends, connection_params={'user': 'root'}).start()
    Put(backends, ["payload.ipynb"], "/tmp/ipynb/payload.ipynb",
            connection_params={'user': 'root'}).start()

    ract = wait_all_actions((client_cmd, lb_cmd, backends_cmd), timeout=600)

    if len(ract) < 3:
        raise Exception("Command execution timeouted")
    ract = any([not act.ok for act in (client_cmd, lb_cmd, backends_cmd)])
    if ract:
        raise Exception("Command execution failed")


def configure_scenario(client, lb, backends):

    logger.info("Configuring node for scenario execution...")

    instance_per_backend = get_host_attributes(backends[0])['architecture']['nb_cores']

    backends_cmd = Remote(";".join((
            "docker run -d --mount type=bind,source=/tmp/ipynb,target=/opt,readonly" \
            " -p %d:5000 --cpuset-cpus=%d --name='%s'" \
            " jupyter/nbviewer python3 -m nbviewer --debug --no-cache --localfiles='/opt'"
                % (8000+cpu, cpu, "notebook-%d" % cpu)
                for cpu in range(instance_per_backend))),
        backends, connection_params={'user': 'root'}).start()

    lb_config = textwrap.dedent("""
        upstream myapp1 {
            %s
        }
    
        server {
            listen 80;
    
            location / {
                proxy_pass http://myapp1;
            }
        }
    """ % "\n".join(
        ["server %s:%d;" % (h.address, 8000+cpu) \
            for h in backends for cpu in range(instance_per_backend)]
        ))

    lb_cmd = Remote(";".join((
            "echo '%s' > /etc/nginx/sites-enabled/lb.conf" % lb_config,
            "rm -f /etc/nginx/sites-enabled/default",
            "systemctl restart nginx")),
        [lb], connection_params={'user': 'root'}).start()

    ract = wait_all_actions((lb_cmd, backends_cmd), timeout=360)

    if len(ract) < 2:
        raise Exception("Command execution timeouted")
    ract = any([not act.ok for act in (lb_cmd, backends_cmd)])
    if ract:
        raise Exception("Command execution failed")


def start_scenario(client, lb, backends, num_clients=10, duration=60):

    logger.info("Running scenario...")

    client_cmd = Remote(
            "locust --only-summary --print-stats --no-web -f /opt/locustfile.py" \
            " --csv=out.csv -c %d -r %d -H http://%s/localfile/payload.ipynb" \
            % (num_clients, num_clients, lb.address),
        [client], connection_params={'user': 'root'}).start()

    client_cmd.wait(duration)
    client_cmd.kill()


def collect_results(client, lb, backends, result_dir, scenario_duration=60):

    if not os.path.exists(result_dir):
        os.makedirs(result_dir)

    logger.info("Collecting scenario results...")

    Get([client],
        ["out.csv_distribution.csv", "out.csv_requests.csv"], result_dir,
        connection_params={'user': 'root'}).start()

    for metric_name in ("power", "load_one"):
        metric_val = get_hosts_metric([lb]+backends, metric_name, resolution=1,
                         from_ts=time.time()-scenario_duration, to_ts=time.time())
    
        with open(os.path.join(result_dir, metric_name+".csv"), "w") as f:
            f.write("#TIME,LB,%s\n" %
                        ",".join(["B%d" % n for n in range(len(backends))]))
            for n in range(min([len(metric_val[h]['values']) for h in [lb]+backends])):
                if metric_val[lb]['values'][n][1] is None: break
                f.write("%d," % metric_val[lb]['values'][n][0])
                f.write("%d," % metric_val[lb]['values'][n][1])
                f.write("%s\n" % ",".join(["%d " % metric_val[h]['values'][n][1]
                                                       for h in backends]))



def clean_scenario(client, lb, backends):

    logger.info("Cleaning nodes after scenario execution...")

    backends_cmd = Remote(" && ".join((
                "docker stop -t 10 $(docker ps -a -q)",
                "docker rm -f $(docker ps -a -q)")),
            backends, connection_params={'user': 'root'}).start()

    ract = wait_all_actions((backends_cmd,), timeout=360)

    if len(ract) < 1:
        raise Exception("Command execution timeouted")
    ract = any([not act.ok for act in (backends_cmd,)])
    if ract:
        raise Exception("Command execution failed")


def parse_results(result_list):
    import pandas as pd
    res_dt = None
    for num_clients, num_backends, directory in result_list:
        dt = pd.read_csv(os.path.join(directory,'out.csv_requests.csv') )
        dt['Failure rate'] = dt['# failures'] / dt['# requests']
        dt['# clients'] = num_clients
        dt['# backend nodes'] = num_backends

        ddt = pd.read_csv(os.path.join(directory,'out.csv_distribution.csv') )
        dt = pd.merge(dt, ddt, left_index=True, right_index=True)

        dt = dt[dt['Method'] == 'GET']

        pdt = pd.read_csv(os.path.join(directory,'power.csv'))
        dt['Average power consumption for load balancer and backend nodes'] = \
            pdt[[c for c in pdt.columns if c != '#TIME']].sum(axis=1).mean()
        ldt = pd.read_csv(os.path.join(directory,'load_one.csv'))
        dt['Average backend nodes load'] = \
            ldt[[c for c in ldt.columns if c not in ('#TIME', 'LB')]].mean().mean()

        if res_dt is None:
            res_dt = dt
        else:
            res_dt = res_dt.append(dt, ignore_index=True)

    return res_dt


def plot_resptime(res_dt):
    import matplotlib.pyplot as plt
    import numpy as np

    plt.figure()
    plt.subplot()
    colors = iter(plt.cm.gist_rainbow(np.linspace(0, 1, len(res_dt['# backend nodes'].unique()))))
    for num_backends in res_dt['# backend nodes'].unique():
        color = next(colors)
        plt.plot(res_dt[res_dt['# backend nodes'] == num_backends]['# clients'],
                 res_dt[res_dt['# backend nodes'] == num_backends]['Average response time'],
                 label='%d backend nodes, average response time' % num_backends,
                 color=color,  linewidth=2.0, linestyle="-", marker='o')
        plt.plot(res_dt[res_dt['# backend nodes'] == num_backends]['# clients'],
                 res_dt[res_dt['# backend nodes'] == num_backends]['95%'],
                 label='%d backend nodes, 95th percentile' % num_backends,
                 color=color,  linewidth=1.0, linestyle="--")
    plt.ylim(ymin=100)
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel('# clients')
    plt.ylabel('Response time (ms)')
    plt.legend()
    plt.show()


def plot_powercons(res_dt):
    import matplotlib.pyplot as plt
    import numpy as np
    import pandas as pd

    plt.figure()
    plt.subplot()

    full_width = 0.6
    bar_width = full_width/len(res_dt['# backend nodes'].unique())
    widthes = iter(np.linspace(0, 1, len(res_dt['# backend nodes'].unique())))

    colors = iter(plt.cm.gist_rainbow(np.linspace(0, 1, len(res_dt['# backend nodes'].unique()))))

    for num_backends in res_dt['# backend nodes'].unique():
        color = next(colors)
        width_shift = -full_width/2+full_width*next(widthes)
        xdata = width_shift + pd.Series(range(len(res_dt[res_dt['# backend nodes'] == num_backends]['# clients'])))
        plt.bar(xdata,
            list(res_dt[res_dt['# backend nodes'] == num_backends]\
                    ['Average power consumption for load balancer and backend nodes']),
                 label='%d backend nodes' % num_backends,
                 color=color, width=bar_width)
    plt.ylim(ymin=0)
    plt.xticks(
        range(len(res_dt[res_dt['# backend nodes'] == num_backends]['# clients'])),
        list(res_dt[res_dt['# backend nodes'] == num_backends]['# clients'])
        )
    plt.xlabel('# clients')
    plt.ylabel('Average cumulated power consumption')
    plt.legend()
    plt.show()

def run_experiment():

    existing_jobsite=(int(sys.argv[1]), "lyon") if len(sys.argv)>1 else None

    num_clients_p = [10, 100, 1000, 10000]
    num_backends_p = [1, 3, 5, 10]

    (client, lb, backends) = reserve_resources(
                                 existing_jobsite=existing_jobsite,
                                 num_backends=max(num_backends_p)
                                 )
    install_resources(client, lb, backends)

    for num_clients, num_backends in itertools.product(num_clients_p, num_backends_p):
        configure_scenario(client, lb, backends[:num_backends])
        start_scenario(client, lb, backends[:num_backends], num_clients)
        collect_results(client, lb, backends[:num_backends],
                os.path.expanduser("~/jrxp/c%d-b%d" % (num_clients, num_backends)))
        clean_scenario(client, lb, backends[:num_backends])

    res_dt = parse_results([(num_clients, num_backends, "~/jrxp/c%d-b%d" % (num_clients,num_backends))
        for num_clients, num_backends in itertools.product(num_clients_p, num_backends_p)])
    res_dt.to_csv('res_dt.csv')

    #plot_resptime(res_dt)
    #plot_powercons(res_dt)


if __name__ == "__main__":
    run_experiment()
